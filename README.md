## Author Info
- **Name:** Melissa Louise Bangloy
- **Student Number** 1438659
- **Course Number** 420-440-DW Infrastructure III (Linux II)
- *

## Assignment:
	The goal of this assignment is to create a Docker container for a previous 320 course assignment and deploy it using both Netlify and Azure.

## Application:
	The application allows user to check the exchange rate of 2 currencies

## Instructions:
   **How to Build the Container**:
	
	1. Clone this repository:
		git clone https://gitlab.com/melissa_louise/linux_assignment5.git	
		
	2. Change directory:
		cd linux_assignment5
		
	3. Build the Docker image:
		docker build -t <image_name>:1.0 .

	4. Verify the built images:
		docker images
		
	5. Run the image locally:
		docker run -d -p <port>:80 -t <image_name> 
		
	OPTIONAL
	6. Tag and push the image to a Docker registry:
		docker tag <app_name>:1.0 <dockerID>/<image_name>:1.0
		docker push <dockerID>/<image_name>:1.0

	7. To verify if the image is pushed to your Docker account:
		docker images -f reference="$DOCKERID/*"
		
		**make sure to set your $DOCKERID 
			export DOCKERID=<your docker id>
			echo $DOCKERID			


  **How to Run the Command from Docker Hub**:
	
	1. Pull the Docker Image:
    			docker pull <image_name>:<tag>
		
	2. Run the Command from the Docker Image:
		docker run -d -p <port>:80 -t <image_name>:<tag>
