# dockerfile for 320 assignment (exhange_rate app)
# 2024-5-17
# BUILD   docker build -t exchange_rate .
# RUN     docker run -d -p 8005:80  -t exchange_rate
# USE     http://localhost:8005

FROM httpd:2.4
LABEL  maintainer="Melissa Bangloy" email="melissa.bangloy@dawsoncollege.qc.ca" modified="2024-05-17"
WORKDIR   /usr/local/apache2/htdocs
COPY app/ ./
