document.addEventListener("DOMContentLoaded", function () {
    // Form fields 
    let from_currency = document.getElementById("from_currency");
    let to_currency = document.getElementById("to_currency");
    let amount = document.getElementById("amount");

    // Buttons 
    let convert = document.getElementById("convert");
    let show = document.getElementById("show");
    let clearHistory = document.getElementById("clear");

    // Object 
    let urlObject = {
        mainURL: "https://api.exchangerate.host/convert?access_key=287b44a0d9d760744b860404bc3ab9e8",
        fromQry: "CAD", // Set default value to "CAD"
        toQry: "CAD",   // Set default value to "CAD"
        amountQry: amount.value,
        absoluteURL: function () {
            return `${this.mainURL}&from=${this.fromQry}&to=${this.toQry}&amount=${this.amountQry}`;
        }
    };

    // Fetches the from and To currency options from the JSON file
    fromToCurrencyOptions();

    function fromToCurrencyOptions() {
        let jsonFile = "json/currency.json";
        fetch(jsonFile)
            .then(response => response.json())
            .then(response => {
                displayOptions(response);
            })
            .catch(error => {
                console.error(error);
                alert("Failed to fetch currency data");
            });
    }

    // This function populates the necessary option for from/to currency
    function displayOptions(response) {
        let currencyKeys = Object.keys(response);

        // Set default options for CAD
        let defaultCurrency = response["CAD"];
        let defaultFromOption = createOption(defaultCurrency.code, defaultCurrency.name);
        let defaultToOption = createOption(defaultCurrency.code, defaultCurrency.name);

        from_currency.appendChild(defaultFromOption);
        to_currency.appendChild(defaultToOption);

        // Add other currencies to the options
        currencyKeys.forEach(key => {
            if (key !== "CAD") {
                let currencyValue = response[key];
                let fromOption = createOption(currencyValue.code, currencyValue.name);
                let toOption = createOption(currencyValue.code, currencyValue.name);

                from_currency.appendChild(fromOption);
                to_currency.appendChild(toOption);
            }
        });
    }

    function createOption(value, text) {
        let option = document.createElement("option");
        option.value = value;
        option.textContent = text;
        return option;
    }

    // Event listeners for form field changes
    from_currency.addEventListener("change", function () {
        urlObject.fromQry = from_currency.value;
    });

    to_currency.addEventListener("change", function () {
        urlObject.toQry = to_currency.value;
    });

    amount.addEventListener("input", function () {
        urlObject.amountQry = amount.value;
    });

    let currentSessionData = [];

    // Event listener for convert button
    convert.addEventListener("click", function () {
        fetch(urlObject.absoluteURL())
            .then(response => response.json())
            .then(response => {
                currentSessionData.push(response);
                getCurrentResult(response);
            })
            .catch(error => {
                console.error(error);
                alert("Failed to fetch conversion data");
            });
    });
    let convertAccessedDate = ""; //stores the date when the result was accessed
    // Function to handle conversion result
    function getCurrentResult(response) {
        let resultFromFetch = {
            from: response.query.from, 
            to: response.query.to,
            rate: response.info.quote,
            amount: response.query.amount, 
            payment: response.result.toFixed(2),
            date: new Date().toLocaleString()
        };

        let resultsObj = JSON.parse(localStorage.getItem("resultsObj")) || [];
        resultsObj.push(resultFromFetch);
        localStorage.setItem("resultsObj", JSON.stringify(resultsObj));
        displayResultToTable(resultsObj);
        console.log(resultFromFetch);
    }
    

    // Function to display results in table
    let tbody = document.getElementsByTagName("tbody");
    function displayResultToTable(results){
        tbody[0].textContent = " ";
        for (let i = 0; i < results.length; i++){
            let tr = document.createElement("tr");

            let tdValue = Object.values(results[i]);
            //console.log(tdValue[0]);
            
            let fromTd = document.createElement("td");
            let toTd = document.createElement("td");
            let rateTd = document.createElement("td");
            let amountTd = document.createElement("td");
            let paymentTd = document.createElement("td");
            let dateTd = document.createElement("td");

            let fromTdContent = document.createTextNode(tdValue[0]);
            let toTdContent  = document.createTextNode(tdValue[1]);
            let rateTdContent  = document.createTextNode(tdValue[2]);
            let amountTdContent  = document.createTextNode(tdValue[3]);
            let paymentTdContent  = document.createTextNode(tdValue[4]);
            let dateTdContent  = document.createTextNode(tdValue[5]);

            fromTd.appendChild(fromTdContent);
            toTd.appendChild(toTdContent);
            rateTd.appendChild(rateTdContent);
            amountTd.appendChild(amountTdContent);
            paymentTd.appendChild(paymentTdContent);
            dateTd.appendChild(dateTdContent);

            tr.appendChild(fromTd);
            tr.appendChild(toTd);
            tr.appendChild(rateTd);
            tr.appendChild(amountTd);
            tr.appendChild(paymentTd);
            tr.appendChild(dateTd);
            tbody[0].appendChild(tr);
        }
    } //displayResultToTable
    

    // Event listener for show button
    show.addEventListener("click", function () {
        let resultsObj = JSON.parse(localStorage.getItem("resultsObj")) || [];
        if (resultsObj.length > 0) {
            displayResultToTable(resultsObj);
        } else {
            alert("No conversion history to display");
        }
    });

    // Event listener for clear button
    clearHistory.addEventListener("click", function () {
        let resultsObj = JSON.parse(localStorage.getItem("resultsObj")) || [];
        if (resultsObj.length > 0) {
            let confirmDelete = confirm(`Confirm to delete conversion history. There are currently ${resultsObj.length} history saved`);
            if (confirmDelete) {
                localStorage.clear();
                displayResultToTable([]);
                alert("Conversion history cleared");
            }
        } else {
            alert("Nothing to delete");
        }
    });
}, false);
